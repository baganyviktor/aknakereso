#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <QTimer>
#include <QIcon>
#include <QPixmap>

struct columndata
{
    bool shown = false;
    int value = 0;
};

class GameManager : public QObject
{
    Q_OBJECT
public:
    GameManager(int time);
    ~GameManager();
    enum GameOverData { TIMEOVER_F, TIMEOVER_S, FIRSTPLAYERWIN, SECPLAYERWIN, WIN };
    bool canPlay() const { return _timer->isActive(); }
    bool isWin();

public slots:
    void newGame(int size);
    void stepGame(int x, int y);

private slots:
    void timerTimeOut();

signals:
    void showButton(int x, int y, int num);
    void showNullButton(int x, int y);
    void gameOver(GameManager::GameOverData g);
    void Tick(int remainingtime);

private:
    columndata** dataTable;
    int gameTime;
    int playerid;
    int timelimit;
    int mapsize;

    QTimer* _timer;
};

#endif // GAMEMANAGER_H
