#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QVector>
#include <QHBoxLayout>
#include <QMessageBox>
#include "gamemanager.h"

class GameWindow : public QWidget
{
    Q_OBJECT

public:
    explicit GameWindow(QWidget *parent = 0);
    ~GameWindow();
    void ShowButtons(int n);

private slots:
    void startNewGame();
    void buttonClicked();
    void gameManager_showButton(int x, int y, int num);
    void gameManager_Tick(int n);
    void gameManager_GameOver(GameManager::GameOverData g);

private:
    GameManager* gameLogic;

    QPushButton* _smallGameButton;
    QPushButton* _middleGameButton;
    QPushButton* _largeGameButton;
    QLabel* time;
    QGridLayout* buttonContainer;
    QMessageBox msgBox;
    QVBoxLayout* mainView;
    QVector<QVector<QPushButton*> > buttonData;
};

#endif // GAMEWINDOW_H
