#include "gamewindow.h"
#include <QMessageBox>
#include <QApplication>

GameWindow::GameWindow(QWidget *parent): QWidget(parent)
{
    setFixedSize(1000,1000);
    setWindowTitle(trUtf8("Aknakereső"));

    gameLogic = new GameManager(30);
    connect(gameLogic, SIGNAL(showButton(int,int,int)), this, SLOT(gameManager_showButton(int, int, int)));
    connect(gameLogic, SIGNAL(gameOver(GameManager::GameOverData)), this, SLOT(gameManager_GameOver(GameManager::GameOverData)));
    connect(gameLogic, SIGNAL(Tick(int)),this,SLOT(gameManager_Tick(int)));

    _smallGameButton = new QPushButton(trUtf8("Könnyű játék (6)"));
    connect(_smallGameButton, SIGNAL(clicked()), this, SLOT(startNewGame()));
    _middleGameButton = new QPushButton(trUtf8("Közepes játék (10)"));
    connect(_middleGameButton, SIGNAL(clicked()), this, SLOT(startNewGame()));
    _largeGameButton = new QPushButton(trUtf8("Nehéz játék (16)"));
    connect(_largeGameButton, SIGNAL(clicked()), this, SLOT(startNewGame()));

    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->addWidget(_smallGameButton);
    hLayout->addWidget(_middleGameButton);
    hLayout->addWidget(_largeGameButton);

    QHBoxLayout* timelayout = new QHBoxLayout();
    time = new QLabel("");
    time->setAlignment(Qt::AlignHCenter);
    time->setMaximumHeight(30);
    timelayout->addWidget(time);
    time->hide();

    buttonContainer = new QGridLayout();
    buttonData.resize(16);
    for(int i=0; i<16;i++)
    {
        buttonData[i].resize(16);
        for(int j=0; j<16;j++)
        {

            buttonData[i][j] = new QPushButton();
            buttonData[i][j]->setFont(QFont("Times New Roman", 60, QFont::Bold));
            buttonData[i][j]->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
            buttonData[i][j]->hide();
            buttonContainer->addWidget(buttonData[i][j],i,j);
            connect(buttonData[i][j],SIGNAL(clicked(bool)),this,SLOT(buttonClicked()));

        }
    }


    mainView = new QVBoxLayout();
    mainView->addLayout(hLayout);
    mainView->addLayout(timelayout);
    mainView->addLayout(buttonContainer);

    setLayout(mainView);
}

GameWindow::~GameWindow()
{
    delete gameLogic;
}


void GameWindow::startNewGame()
{
    if (QObject::sender() == _smallGameButton) // attól függ, melyik gombra kattintottunk
    {
        gameLogic->newGame(6); // létrehozzuk a megfelelõ méretû játékot
        ShowButtons(6);
    }
    else if (QObject::sender() == _middleGameButton)
    {
        gameLogic->newGame(16);
        ShowButtons(10);
    }
    else if(QObject::sender() == _largeGameButton)
    {
        gameLogic->newGame(16);
        ShowButtons(16);
    }
    time->show();
}

void GameWindow::ShowButtons(int n)
{
    for(int i=0; i<16; i++)
    {
        for(int j=0; j<16; j++)
        {
            buttonData[i][j]->hide();
        }
    }
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<n; j++)
        {
            buttonData[i][j]->show();
            buttonData[i][j]->setText("");
        }
    }
}

void GameWindow::buttonClicked()
{
    if (!gameLogic->canPlay()) return;

    QPushButton* senderButton = dynamic_cast <QPushButton*> (QObject::sender());
    int location = buttonContainer->indexOf(senderButton);
    int x = location / 16;
    int y = location % 16;
    gameLogic->stepGame(x,y);
    if(gameLogic->isWin()) gameManager_GameOver(GameManager::GameOverData::WIN);
}

void GameWindow::gameManager_showButton(int x, int y, int num)
{
    if(num == -1)
        buttonData[x][y]->setText("X");
    else
        buttonData[x][y]->setText(QString::number(num));
}

void GameWindow::gameManager_Tick(int n)
{
    time->setText(QString(trUtf8("Hátralévő idő: %1s ")).arg(n));
}

void GameWindow::gameManager_GameOver(GameManager::GameOverData g)
{
    if(g == GameManager::GameOverData::TIMEOVER_S)
    {
        msgBox.setText(trUtf8("Sajnálom, lejárt az idő! Az első játékos nyert!"));
        msgBox.exec();
    }
    else if(g == GameManager::GameOverData::TIMEOVER_F)
    {
        msgBox.setText(trUtf8("Sajnálom, lejárt az idő! A második játékos nyert!"));
        msgBox.exec();
    }
    else if(g == GameManager::GameOverData::FIRSTPLAYERWIN)
    {
        msgBox.setText(trUtf8("Az első játékos nyert!"));
        msgBox.exec();
    }
    else if(g == GameManager::GameOverData::SECPLAYERWIN)
    {
        msgBox.setText(trUtf8("A második játékos nyert!"));
        msgBox.exec();
    }
    else if(g == GameManager::GameOverData::WIN)
    {
        msgBox.setText(trUtf8("Döntetlen az állás!"));
        msgBox.exec();
    }
    time->hide();
}

