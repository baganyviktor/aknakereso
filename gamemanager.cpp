#include "gamemanager.h"

#include <QTime>
#include <time.h>
#include <stdlib.h>
#include <QDebug>

GameManager::GameManager(int _time)
{
    timelimit = _time;
    _timer = new QTimer();
    _timer->setInterval(1000);
    _timer->setSingleShot(false);
    connect(_timer, SIGNAL(timeout()), this, SLOT(timerTimeOut()));
    dataTable = new columndata*[16];
    for(int i=0; i<16;i++)
    {
        dataTable[i] = new columndata[16];
    }
    srand(time(NULL));
    gameTime = timelimit;
}

GameManager::~GameManager()
{
    _timer->stop();
    delete _timer;
}

void GameManager::newGame(int size)
{
    if (_timer->isActive()) _timer->stop();

    mapsize = size;
    gameTime = timelimit;
    playerid = 1;

    for(int i=0; i<16; i++)
    {
        for(int j=0; j<16; j++)
        {
            dataTable[i][j].value = 0;
            dataTable[i][j].shown = false;
        }
    }
    int bombak = 0;
    int hanyvan = 0;
    while(hanyvan < (size==6?6:size==10?30:size==16?70:0))
    {
        int x = rand()%size;
        int y = rand()%size;
        if(dataTable[x][y].value != -1)
        {
            dataTable[x][y].value = -1;
            hanyvan++;
        }
    }
    for(int i=0; i<16; i++)
    {
        for(int j=0; j<16; j++)
        {
            if(dataTable[i][j].value != -1)
            {
                if(i+1 < size && j+1 < size)
                    if(dataTable[i+1][j+1].value == -1)
                       ++bombak;
                if(i-1 >= 0 && j-1>= 0)
                    if(dataTable[i-1][j-1].value == -1)
                        ++bombak;
                if(i-1 >= 0 && j+1 < size)
                    if(dataTable[i-1][j+1].value == -1)
                        ++bombak;
                if(i+1 < size && j-1 >= 0)
                    if(dataTable[i+1][j-1].value == -1)
                        ++bombak;

                /////////////////////////////////

                if(i+1 < size && j < size)
                    if(dataTable[i+1][j].value == -1)
                       ++bombak;
                if(i >= 0 && j-1>= 0)
                    if(dataTable[i][j-1].value == -1)
                        ++bombak;
                if(i-1 >= 0 && j < size)
                    if(dataTable[i-1][j].value == -1)
                        ++bombak;
                if(i < size && j+1 < size)
                    if(dataTable[i][j+1].value == -1)
                        ++bombak;
                dataTable[i][j].value = bombak;
                bombak = 0;
            }
        }
    }
    _timer->start();
}

void GameManager::stepGame(int x, int y)
{
    if(dataTable[x][y].shown) return; //ha már kezelve volt nem kezeljük újra

    if(playerid == 1)
        playerid = 2;
    else playerid = 1;

    if(dataTable[x][y].value == -1)
    {
        _timer->stop();

        dataTable[x][y].shown = true;
        emit showButton(x,y,dataTable[x][y].value);
        emit gameOver(playerid==1?GameOverData::FIRSTPLAYERWIN:GameOverData::SECPLAYERWIN);
    }
    else
    {
        gameTime = timelimit;

        if(dataTable[x][y].value == 0)
        {
            qDebug() << x << " " << y;
            dataTable[x][y].shown = true;
            if(x+1 < mapsize)
            {
                //showButton(x+1,y,dataTable[x+1][y].value);
                //dataTable[x+1][y].shown = true;
                stepGame(x+1,y);
            }
            if(y-1>= 0)
            {
                //showButton(x,y-1,dataTable[x][y-1].value);
                //dataTable[x][y-1].shown = true;
                stepGame(x,y-1);
            }
            if(x-1 >= 0)
            {
                //showButton(x-1,y,dataTable[x-1][y].value);
                stepGame(x-1,y);
            }
            if(y+1 < mapsize)
            {
                //showButton(x,y+1,dataTable[x][y+1].value);
                stepGame(x,y+1);
            }
            emit showButton(x,y,dataTable[x][y].value);
        }
        else
        {
            dataTable[x][y].shown = true;
            emit showButton(x,y,dataTable[x][y].value);
        }
    }
}

bool GameManager::isWin()
{
    for(int i=0; i<mapsize; ++i)
    {
        for(int j=0; j<mapsize; ++j)
        {
            if(dataTable[i][j].value != -1 && !dataTable[i][j].shown)
                return false;
        }
    }
    return true;
}

void GameManager::timerTimeOut()
{
    if(--gameTime > 0)
        emit Tick(gameTime);
    else {
        emit gameOver(playerid==1?GameOverData::TIMEOVER_F:GameOverData::TIMEOVER_S);
        _timer->stop();
    }
}
